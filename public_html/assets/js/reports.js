$(document).ready(function(){
	$('#reception_date').datepicker({
		dateFormat : 'dd/mm/yy'
	});
	if ($('input[type="radio"].month').is(':checked')){
		init_month_frame();
	}
	if ($('input[type="radio"].range').is(':checked')){
		init_range_frame();
	}
	$('input[type="radio"].range').click(function(){
		init_range_frame();
	});
	$('input[type="radio"].month').click(function(){
		init_month_frame();
	});
});
function init_month_frame(){
	$('.frame-range').fadeOut(100, function(){
		$('.frame-month').fadeIn(100);
	});
	$('.frame-month select').attr('required','required');
	$('.frame-range select').removeAttr('required');
}
function init_range_frame(){
	$('.frame-month').fadeOut(100, function(){
		$('.frame-range').fadeIn(100);
	});
	$('.frame-range select').attr('required','required');
	$('.frame-month select').removeAttr('required');
}

