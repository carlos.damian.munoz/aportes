$loading_pages = false;
$loaded_pages = 0;
$new_pages_batch = true;
$elementsInProcess = 0;
$elementsProcessed = 0;

$(document).ready(function(){

	load_pages();

	$('#reception_from, #reception_to, #transference_from, #transference_to').datepicker({
		dateFormat : 'dd/mm/yy',
		maxDate: '0'
	});
	
	if ($('#selected-filters .filter').length > 0){
		$('#mother_checkbox').css('display','inline');
		$('#mother_checkbox').click(function(){
			if ($(this).is(':checked')){
				$('.element input[type="checkbox"]').prop('checked',true);
			}
			else{
				$('.element input[type="checkbox"]').prop('checked',false);
			}
		});
	}

	$('.filters-button').click(function(){
		$('#filters').slideToggle(50);
		$('#selected-filters').fadeToggle(50);
	});

	$('#selected-filters .filter span.close').click(function(){
		$filters_url = ""
		$(this).parent().siblings().each(function(){
			$filters_url += '_' + $(this).find('.filter-entry').val();
		});
		$page = $('#page_name').val();
		$basic_url = '/'+$page;
		if ($filters_url != ""){
			$basic_url += '/f';
		}
		window.location.href = $basic_url + $filters_url + '/o_id_desc';
	});

	$('.elements-header span').click(function(){
		$order_direction = "asc";
		if ( $('#order_field').val() == $(this).attr('class') && $('#order_direction').val() == "asc"){
			$order_direction = "desc";
		}
		$url = '/'+$('#page_name').val();
		if ($('#order_url').length > 0){
			$url = $('#order_url').val();
		}
		window.location.href = $url + '/o_' + $(this).attr('class') + '_' + $order_direction;
	});

	$('.filter-action').click(function(){
		$filters_url = "";
	
		if ($('.filter.collector').length > 0 && $('.filter.collector input.user_input').val() != ""){
			$filters_url += '_' + $('.filter.collector input.url_name').val() + "_" + encodeURIComponent( $('.filter.collector input.user_input').val() );
		}

		if ($('.filter.contributor input.user_input').val() != ""){
			$filters_url += '_' + $('.filter.contributor input.url_name').val() + "_" + encodeURIComponent( $('.filter.contributor input.user_input').val() );
		}

		if ($('.filter.amount input.user_input.from').val() != ""){
			$filters_url += '_' + $('.filter.amount input.url_name').val() + "-min_" + $('.filter.amount input.user_input.from').val();
		}

		if ($('.filter.amount input.user_input.to').val() != ""){
			$filters_url += '_' + $('.filter.amount input.url_name').val() + "-max_" + $('.filter.amount input.user_input.to').val();
		}

		if ($('.filter.period select.month.from').val() != "" && $('.filter.period select.year.from').val() != ""){
			$filters_url += '_' + $('.filter.period input.url_name').val() + "-min"
			$filters_url += '_' + $('.filter.period select.year.from').val() + '-' + $('.filter.period select.month.from').val() + '-1';
		}

		if ($('.filter.period select.month.to').val() != "" && $('.filter.period select.year.to').val() != ""){
			$filters_url += '_' + $('.filter.period input.url_name').val() + "-max"
			$filters_url += '_' + $('.filter.period select.year.to').val() + '-' + $('.filter.period select.month.to').val() + '-1';
		}

		if ($('.filter.receptionDate').length > 0 && $('.filter.receptionDate input.user_input.from').val() != ""){
			$filters_url += '_' + $('.filter.receptionDate input.url_name').val() + '-min';
			$splitted_date = $('.filter.receptionDate input.user_input.from').val().split('/');
			$filters_url += '_' + $splitted_date[2] + '-' + $splitted_date[1] + '-' + $splitted_date[0];
		}

		if ($('.filter.receptionDate').length > 0 && $('.filter.receptionDate input.user_input.to').val() != ""){
			$filters_url += '_' + $('.filter.receptionDate input.url_name').val() + '-max';
			$splitted_date = $('.filter.receptionDate input.user_input.to').val().split('/');
			$filters_url += '_' + $splitted_date[2] + '-' + $splitted_date[1] + '-' + $splitted_date[0];
		}

		if ($('.filter.transferenceDate input.user_input.from').val() != ""){
			$filters_url += '_' + $('.filter.transferenceDate input.url_name').val() + '-min';
			$splitted_date = $('.filter.transferenceDate input.user_input.from').val().split('/');
			$filters_url += '_' + $splitted_date[2] + '-' + $splitted_date[1] + '-' + $splitted_date[0];
		}

		if ($('.filter.transferenceDate input.user_input.to').val() != ""){
			$filters_url += '_' + $('.filter.transferenceDate input.url_name').val() + '-max';
			$splitted_date = $('.filter.transferenceDate input.user_input.to').val().split('/');
			$filters_url += '_' + $splitted_date[2] + '-' + $splitted_date[1] + '-' + $splitted_date[0];
		}
	
		if ($('.filter.status').length > 0 && $('.filter.status select').val() != ""){
			$filters_url += '_' + $('.filter.status input.url_name').val() + "_" + $('.filter.status select').val();
		}

		if ($filters_url != ""){
			window.location.href = '/'+$('#page_name').val()+'/f' + $filters_url + '/o_id_desc';
		}

	});

	$('#set_transfered').click(function(){
		startProcess();
		callback = function($result,$contribution){
			$contribution.find('div.status').removeClass('received').addClass('transfered').html('Entregado');
			$contribution.find('div.transferenceDate').html($result.date);
			endProcessLoop($contribution);
			processNext('received','/aportes/transferir',callback);
		} 
		processNext('received','/aportes/transferir',callback);
	});
	
	$('#set_received').click(function(){
		startProcess();
		callback = function($result,$contribution){
			$contribution.find('div.status').removeClass('transfered').addClass('received').html('Recibido');
			$contribution.find('div.transferenceDate').html('-');
			endProcessLoop($contribution);
			processNext('transfered','/aportes/recuperar',callback);
		} 
		processNext('transfered','/aportes/recuperar',callback);
	});
	
	$('#set_checked').click(function(){
		startProcess();
		callback = function($result,$contribution){
			$contribution.find('div.status').removeClass('transfered').addClass('checked').html('Confirmado');
			endProcessLoop($contribution);
			processNext('transfered','/aportes/confirmar',callback);
		} 
		processNext('transfered','/aportes/confirmar',callback);
	});
	
	$('#set_transfered_back').click(function(){
		startProcess();
		callback = function($result,$contribution){
			$contribution.find('div.status').removeClass('checked').addClass('transfered').html('Entregado');
			endProcessLoop($contribution);
			processNext('checked','/aportes/desconfirmar',callback);
		} 
		processNext('checked','/aportes/desconfirmar',callback);
	});
	
	$('#remove').click(function(){
		$('#remove-confirmation-modal').css('display','flex');
	});

	$('#remove-confirmation-modal button.no').click(function(){
		$('#remove-confirmation-modal').css('display','none');
	});

	$('#remove-confirmation-modal button.yes').click(function(){
		startProcess();
		$('#remove-confirmation-modal').css('display','none');
		callback = function($result,$contribution){
			$contribution.remove();
			endProcessLoop($contribution);
			processNext('received','/aportes/remover',callback);
		} 
		processNext('received','/aportes/remover',callback);
	});

	$(window).scroll(function() {
		if(!$loading_pages && $(window).scrollTop() + $(window).height() == $(document).height()) {
		   load_pages();
		}
	});

	$('.elements-header .checkbox input').click(function(){
		if ($(this).prop('checked')){
			$('input[type="checkbox"]').prop('checked',true);
		}
		else{
			$('input[type="checkbox"]').prop('checked',false);
		}
	});
});

function load_pages(){
	$loading_pages = true;
	if ($loaded_pages < $('#pages_count').val()){
		$('.ajax-loader').show();
		$new_pages_batch = true;
		load_next();
	}
}

function load_next(){
	if( ($loaded_pages % 1 != 0 || $new_pages_batch) && $loaded_pages < $('#pages_count').val()){
		$.ajax({
			url : $('#load_url').val() + "/pag_" + (++$loaded_pages),
			method : "get",
			dataType : "json",
			success : function($result){
				if ($result.success){
					$('#contributions').append($result.content);
					updateShownNumber();
					load_next();
				}
			}
		})
		$new_pages_batch = false;
	}
	else{
		$loading_pages = false;
		showButtons();
		initCheckboxes();
		showOrHideMainCheckbox();
		$('.ajax-loader').hide();
	}
}

function startProcess(){
	setNotification('neutral','Procesando: <span class="progress">0</span>%');
	$elementsInProcess = $('.element input[type="checkbox"]:checked').length;
	$elementsProcessed = 0;
}

function endProcessLoop($contribution){
	$contribution.find('.checkbox input').prop('checked',false);
	$elementsProcessed++;
	$progress = Math.round(100 / $elementsInProcess * $elementsProcessed);
	$('.notification.neutral .progress').html($progress);
}

function endProcess(){
	showButtons();
	$('.notification.neutral').remove();
	showOrHideMainCheckbox();
	$('#contributions .elements-header .checkbox input').prop('checked',false);
	updateShownNumber();
	countTotalResults();
}

function processNext($status,$url,callback){
	if ($('.element input[type="checkbox"]:checked').length > 0){
		$('.element input[type="checkbox"]:checked:first').each(function(){
			$contribution = $(this).parent().parent();
			if ($contribution.find('.status.'+$status).length > 0){
				$id = $contribution.find('input.id').val();
				$.ajax({
					url : $url,
					data : {id : $id},
					method : "post",
					dataType : "json",
					success : function($result){
						if ($result.success){
							callback($result,$contribution);
						}
						else{
						}
					}
				});
			}
			else{
				$contribution.find('.checkbox input').prop('checked',false);
				$elementsProcessed++;
				processNext($status,$url,callback);
			}
		});
	}
	else{
		endProcess();
	}
}

function showButtons(){
	$('.buttons button').hide();
	if ($('.status.received').length > 0){
		$('#set_transfered').show();
		$('#remove').show();
	}
	if ($('.status.transfered').length > 0){
		$('#set_received').show();
		$('#set_checked').show();
	}
	if ($('.status.checked').length > 0){
		$('#set_transfered_back').show();
	}
}

function initCheckboxes(){
	$('.checkbox input').click(function(){
		if ($('.checkbox input:checked').length > 0){
			$('.buttons button').removeAttr('disabled');
		}
		else{
			$('.buttons button').attr('disabled','disabled');
		}
	});
}

function setNotification($type,$content){
	$('.notification.dummy').clone().appendTo('.notifications');
	$('.notifications .notification:last').removeClass('dummy').addClass($type).find('.content').html($content);
}

function updateShownNumber(){
	$count = $('#contributions .element').length;
	$('.results-shown-number').html($count);
}

function countTotalResults(){

	$url = window.location.href;
	$.ajax({
		url: $url,
		method : 'post',
		dataType : 'json',
		data : { count : true },
		success : function($result){
			$('.total-results-number').html($result.count);
		}
	});
	
}

function showOrHideMainCheckbox(){
	if ($('#contributions .element .checkbox input').length > 0){
		$('#contributions .elements-header .checkbox input').show();
	}
	else{
		$('#contributions .elements-header .checkbox input').hide();
	}
}
