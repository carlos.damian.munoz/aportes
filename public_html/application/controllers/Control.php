<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Control extends MY_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	var $status = [
		"names" => [ 'Recibido','Entregado','Confirmado' ],
		'classes' => [ 'received','transfered','checked' ],
		"titles" => [ 
			'significa que el/la recaudador/a recibió el dinero',
			'significa que el/la recaudador/a pasó el dinero a el/la administrador/a',
			'significa que el dinero llegó al parque'
	 	]
	];
	var $month_names = [ 'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre' ]; 

	public function index($historic=false)
	{
		$this->load->model('Control_model','',TRUE);
		$this->load->model('Suggestions_model','',TRUE);
		$this->check_log();

		$this->data['month_names'] = $this->month_names; 
		$this->data['status'] = $this->status;
		$this->data['historic'] = $historic;
		$this->data['contributors'] = $this->Suggestions_model->get_contributors();
		$this->data['user_contributors'] = $this->Suggestions_model->get_contributors($this->data['userdata']['id']);
		$this->data['collectors'] = $this->Suggestions_model->get_collectors();
	
		$this->data['modifier_alias'] = [
			"min" => ": Desde",
			"max" => ": Hasta",
			"" => ":"
		];

		$url = $_SERVER['REQUEST_URI'];

		$order		= $this->get_order($url);
		$filters    = $this->get_filters($url);

		$this->data['order'] = $order;
		if (substr_count($url,'/') < 2){
			$this->data['load_url'] = $url . '/load';
		}
		else{
			$this->data['load_url'] = substr($url,0,strpos($url,'/',strpos($url,'/')+1)+1) . 'load/' . substr($url,strpos($url,'/',strpos($url,'/')+1)+1,strlen($url));
		}
		$this->data['filters'] = $filters;
		$this->data['fields'] = $this->Control_model->fields;
	
 		$count = $this->Control_model->get_contributions($filters,$order,$this->data['userdata']['id'],$this->data['userdata']['role'],0,true,$historic);
		if ($this->input->post('count')){
			echo json_encode(["count"=>$count]);
			return;
		}
		$this->data['total_results_number'] = $count;
		$this->data['pages_count'] = ceil($count / RESULTS_PER_PAGE);

		if ($historic){
			$this->data['stylesheets'] 	= ['historic.css'];
			$this->data['scripts'] 			= ['layout.js'];
		}
		else{
			if ($this->data['userdata']['role'] == ROLES_COLLECTOR){
				$this->data['stylesheets'] 	= ['control.css'];
			}
			elseif ($this->data['userdata']['role'] == ROLES_ADMINISTRATOR){
				$this->data['stylesheets'] 	= ['control_admin.css'];
			}
		}
		$this->data['stylesheets'][] 	= 'list.css';
		$this->data['scripts'] 			= ['layout.js','control.js'];

		$this->layout('control');
	}

	public function load_contributions($historic=false){

		$this->load->model('Control_model','',TRUE);
		$this->check_log();
		$url = $_SERVER['REQUEST_URI'];
		$page = 1;
		if (strpos($url,'/pag_') > -1 ){
			$page = explode('/pag_',$url)[1];
			$url = explode('/pag_',$url)[0];
		}
		$order		= $this->get_order($url);
		$filters    = $this->get_filters($url);
		$this->data['month_names'] = $this->month_names;
		$this->data['status'] = $this->status;
		$this->data['historic'] = $historic;
		$this->data['contributions'] = $this->Control_model->get_contributions($filters,$order,$this->data['userdata']['id'],$this->data['userdata']['role'],$page,false,$historic);;
		$response['success'] = true;
		$response['rows_count'] = count($this->data['contributions']);
		$response['content'] = $this->load->view('contributions',$this->data,true);
		echo json_encode($response);

	}

	public function get_order($url){
		$order = [];
		if (strpos($url,'/o_') > -1 ){
			$order_uri = explode('/o_',$url)[1];
			$url = explode('/o_',$url)[0];
			$tokens = explode('_',$order_uri);
			$order["url"] = $url;
			$order["field"] = $tokens[0];
			$order["direction"] = $tokens[1];
			/*echo '<pre>'; var_dump($order); echo '</pre>';*/
		}
		return $order;	
	}

	public function get_filters($url){
		$filters = [];
		if (strpos($url,'/f_') > -1){
			$filters_uri = explode('/f_',urldecode($url))[1];
			$filters_uri = explode('/o_',urldecode($filters_uri))[0];
			$tokens = explode('_',$filters_uri);
			for($i=0; $i<count($tokens); $i++){
				if($i % 2 == 0){
					if (strpos($tokens[$i],'-min') == strlen($tokens[$i]) - 4 || 
						strpos($tokens[$i],'-max') == strlen($tokens[$i]) - 4){
						$filters[$tokens[$i]]['name'] = substr($tokens[$i],0,$tokens[$i] - 4);
						$filters[$tokens[$i]]['modifier'] = substr($tokens[$i],$tokens[$i] - 3,3);
					}
					else{
						$filters[$tokens[$i]]['name'] = $tokens[$i];
						$filters[$tokens[$i]]['modifier'] = "";
					}
					$filters[$tokens[$i]]['entry'] = $tokens[$i] . '_' .$tokens[$i+1];
				} 
				else{
					$filters[$tokens[$i-1]]['value'] = $tokens[$i];
					if (in_array($filters[$tokens[$i-1]]['name'], ['recepcion','entrega'])){
						$exploded_date = explode('-',$tokens[$i]);
						$filters[$tokens[$i-1]]['show_value'] = str_pad($exploded_date[2],2,"0",STR_PAD_LEFT) . "/" . str_pad($exploded_date[1],2,"0",STR_PAD_LEFT) . "/" . $exploded_date[0];
					} else if ($filters[$tokens[$i-1]]['name'] == 'periodo'){ 
						$exploded_date = explode('-',$tokens[$i]); 
						$filters[$tokens[$i-1]]['show_value'] = $this->month_names[$exploded_date[1]-1] . ' ' . $exploded_date[0]; } else if ($filters[$tokens[$i-1]]['name'] == 'estado'){ $filters[$tokens[$i-1]]['show_value'] = $this->status['names'][$tokens[$i]-1];
					}
					else{
						$filters[$tokens[$i-1]]['show_value'] = $tokens[$i];
					}
				}
			}
		}
		return $filters;
	}

	public function set_transfered(){
		$this->load->model('Control_model','',TRUE);
		if ($this->Control_model->set_transfered($this->input->post('id'))){
			echo json_encode(["success"=>true,"date"=>date('d/m/Y')]);
		}
		else{
			echo json_encode(["success"=>false]);
		}
	}

	public function set_received(){
		$this->load->model('Control_model','',TRUE);
		if ($this->Control_model->set_received($this->input->post('id'))){
			echo json_encode(["success"=>true]);
		}
		else{
			echo json_encode(["success"=>false]);
		}
	}

	public function set_checked(){
		$this->load->model('Control_model','',TRUE);
		if ($this->Control_model->set_checked($this->input->post('id'))){
			echo json_encode(["success"=>true]);
		}
		else{
			echo json_encode(["success"=>false]);
		}
	}

	public function set_transfered_back(){
		$this->load->model('Control_model','',TRUE);
		if ($this->Control_model->set_transfered_back($this->input->post('id'))){
			echo json_encode(["success"=>true]);
		}
		else{
			echo json_encode(["success"=>false]);
		}
	}

	public function remove(){
		$this->load->model('Control_model','',TRUE);
		if ($this->Control_model->remove_contribution($this->input->post('id'))){
			echo json_encode(["success"=>true]);
		}
		else{
			echo json_encode(["success"=>false]);
		}
	}
}
