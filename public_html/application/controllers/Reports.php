<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	var $month_names = [ 'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre' ]; 

	public function index()
	{
		$this->load->model('Reports_model','',TRUE);
		$this->check_log();

		$this->data['month_names'] = $this->month_names;
		$this->data['stylesheets'] 	= ['reports.css'];
		$this->data['scripts'] 		= [];
		if ( !empty($this->input->get()) ){
		
			$post = $this->input->get();
			$month_from = $post['month_from'];
			$year_from 	= $post['year_from'];
			$month_to 	= $post['month_to'];
			$year_to 	= $post['year_to'];

			$period_from 	= $year_from . "-" . str_pad($month_from,2,"0",STR_PAD_LEFT) . "-01";
			$period_to 		= $year_to . "-" . str_pad($month_to,2,"0",STR_PAD_LEFT) . "-02";

			$report = $this->Reports_model->get_report($post['threshold'],$period_from,$period_to);
			$report['highest_month_amount'] = 0;
			foreach($report['months'] as $month){
				if ($month['total_sum'] > $report['highest_month_amount']){
					$report['highest_month_amount'] = $month['total_sum'];
				}
			}
			$this->data['report'] = $report;
			
			$this->data['threshold'] = $post['threshold'];

			$table = $this->load->view('report_table',$this->data,true);
			//$this->layout('report_table',$this->data);
			file_put_contents(APPPATH . '../public/pdf/' . 'report.html', $table);
			$this->layout('report_form');
		}
		else{
			$this->layout('report_form');
		}


	}
}
