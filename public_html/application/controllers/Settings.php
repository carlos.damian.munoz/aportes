<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function change_password()
	{
		$this->load->model('Settings_model','',TRUE);
		$this->load->model('Log_model','',TRUE);
		$this->check_log();

		$this->data['stylesheets'] = ['change_password.css'];
		$this->data['scripts'] = [];
		
		if ( !empty($this->input->post()) ){
			
			$old_password = $this->input->post('password');
			$user_data		= $this->Log_model->check_log_user($this->data['userdata']['email'], $old_password);
			if (!empty($user_data)){
				$new_password = $this->input->post('new_password');
				$repeat_new_password = $this->input->post('repeat_new_password');
				if ($new_password == $repeat_new_password){
					$change_result = $this->Settings_model->change_password($this->data['userdata']['id'],$new_password);
					if ($change_result == 1){
						$this->data['success'] = "Contraseña modificada con éxito";
					}
				}
				else{
					$this->data['error'] = array(
						"message" =>  "Las contraseñas nuevas no coinciden",
						"class" =>  "non-match"
					);
				}
			}
			else{
				$this->data['error'] = array(
					"message" =>  "Contraseña actual incorrecta",
					"class" =>  "wrong-password"
				);
			}
			
		}
		
		$this->layout('change_password');
	}
	

	public function retrieve_password()
	{
		$this->load->model('Settings_model','',TRUE);
		if ( !empty($this->input->post()) ){
			
			$email 	= $this->input->post('email');
			$generation_result	= $this->Settings_model->generate_retrieval_hash($email);
			if ($generation_result['success']){
				$hash = $generation_result['hash'];
				if ($this->send_password_email($email,$hash)){
					$this->session->sent_email_address = $email;
					redirect('/settings/email_sent');
				}
				else{
					$this->session->email_error = true;
					redirect('/settings/email_sent');
				}
			}
			else{
				if ($generation_result['error'] == 'EMAIL_NOT_FOUND'){
					$this->data['error'] = array(
						"message" =>  "El email ingresado no se encuentra en nuestra base de datos, intente de nuevo o contacte al administrador",
						"class" =>  "wrong-email"
					);
				}
			}
		}

		$this->data['stylesheets'] = ['retrieve_password.css'];
		$this->data['scripts'] = [];
		
		$this->layout('retrieve_password');
	}

	public function generate_password(){
		$this->load->model('Settings_model','',TRUE);
		if ( !empty($this->input->get('hash')) ){
			$hash = $this->input->get('hash');
			$generation_result = $this->Settings_model->generate_random_password($hash);
			if ($generation_result['success']){
				$this->data['new_password'] = $generation_result['new_password'];
			}
			else{
				if ($generation_result['error'] == 'HASH_NOT_FOUND'){
					$this->data['error'] = array(
						"message" =>  "Link no válido",
						"class" =>  "wrong-hash"
					);
				}
			}
		}
		$this->data['stylesheets'] = ['retrieve_password.css'];
		$this->data['scripts'] = [];
		$this->layout('generate_password');
	}

	private function send_password_email($email,$hash){
		$email_template = $this->load->view('retrieve_password_email',["hash"=>$hash],true);
		$this->data['email_template'] = $email_template;
		$mail = new Mail();
		$mail->IsSMTP(); // enable SMTP
		$mail->IsHTML(true);
		$mail->Subject = "Reestablecer contraseña";
		$mail->Body = $email_template;
		$mail->AddAddress("gonzalezrojosantiago@gmail.com");

		 if(!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
			return false;
		 } else {
			return true;
		 }	
	}

	public function email_sent(){
		$this->data['stylesheets'] = ['retrieve_password.css'];
		$this->data['scripts'] = [];
		if (isset($this->session->sent_email_address)){
			$this->data['email'] = $this->session->sent_email_address;
			unset($_SESSION['sent_email_address']);
		}
		else if (isset($this->session->email_error)){
			$this->data['email_error'] = "Ha habido un error intentando enviar el mail, por favor contacte a los administradores";
			unset($_SESSION['email_error']);
		}
		else{
			redirect('/login');
		}
		$this->layout('retrieve_password');
	}
}
