<?php 
class Control_model extends CI_Model {

	public $fields = [
		"id" => [
			"field_name" => "c.id"
		],
		"user_id" => [
			"field_name" => "user_id"
		],
		"recaudador" => [
			"field_name" => "concat(u.name,' ', u.last_name)",
			"order_fields" => ["u.name","u.last_name"],
			"show_name" => "Recaudador",
			"special_operator" => "like",
		],
		"aportante" => [
			"field_name" => "cr.name",
			"order_fields" => ["cr.name"],
			"show_name" => "Aportante",
			"special_operator" => "like"
		],
		"monto" => [
			"field_name" => "amount",
			"show_name" => "Monto"
		],
		"periodo" => [
			"field_name" => "period",
			"show_name" => "Período"
		],
		"recepcion" => [
			"field_name" => "reception_date",
			"show_name" => "Fecha de recepción"
		],
		"entrega" => [
			"field_name" => "transference_date",
			"show_name" => "Fecha de entrega"
		],
		"estado" => [
			"field_name" => "status",
			"show_name" => "Estado"
		]
	];

	public $operators = [
		"min" => ">=",
		"max" => "<=",
		"" => ""
	];

	public function get_contributions($filters,$order,$user_id,$role,$page,$count,$historic=false){

		$this->db->start_cache();
		$this->db->from('contributions c');
		if ($role == ROLES_ADMINISTRATOR || $historic){
			$select = 'c.id,concat(u.name," ",u.last_name) as collector,cr.name as contributor,amount,month(period) as period_month, year(period) as period_year,date_format(reception_date,"%d/%m/%Y") as fecha_recepcion,if(transference_date > "1970-01-01", date_format(transference_date,"%d/%m/%Y"),"-") as fecha_transferencia,status'; 
			$this->db->join('contributors cr','cr.id = c.contributor_id');
			$this->db->join('users u','u.id = c.user_id');
			if ($historic){
				$this->db->where('status =',STATUS_CHECKED);
			}
			else{
				$this->db->where('status !=',STATUS_RECEIVED);
			}
		}
		else{
			$select = 'c.id,cr.name as contributor,amount,month(period) as period_month, year(period) as period_year,date_format(reception_date,"%d/%m/%Y") as fecha_recepcion,if(transference_date > "1970-01-01", date_format(transference_date,"%d/%m/%Y"),"-") as fecha_transferencia,status';
			$this->db->join('contributors cr','cr.id = c.contributor_id');
			$this->db->where('user_id',$user_id);
		}

		foreach ($filters as $filter){
			$value =  $filter["value"];
			if (isset($this->fields[$filter['name']]['special_operator']) && $this->fields[$filter['name']]['special_operator'] == "like"){
				$key = $this->fields[$filter['name']]["field_name"];
				$this->db->like($key, $value);
			}
			else{
				$key = 'c.'.$this->fields[$filter['name']]["field_name"] . " " . $this->operators[$filter['modifier']];
				$this->db->where($key, $value);
			}
			if (isset($this->fields[$filter['name']]['join_table'])){
				$this->db->join($this->fields[$filter['name']]['join_table'],$this->fields[$filter['name']]['join_table']);
			}
		}
		if (!empty($order)){
			if (isset($this->fields[$order['field']]['order_fields'])){
				foreach($this->fields[$order['field']]['order_fields'] as $order_field){
					$this->db->order_by($order_field,$order['direction']);
				}
			}
			else{
				$this->db->order_by($this->fields[$order['field']]['field_name'],$order['direction']);
			}
		}

		$this->db->stop_cache();

		if ($count){
			return $this->db->select('count(*) as count')->get()->row_array()['count'];
		}
		else{
			return $this->db->select($select)->limit(RESULTS_PER_PAGE, ($page-1)*RESULTS_PER_PAGE)->get()->result_array();
		}

	}
		
	public function set_transfered($id){
		$this->db->set('transference_date', date('Y-m-d H:i:s'));
		$this->db->set('status',STATUS_TRANSFERED);
		$this->db->where('id',$id);
		$this->db->where('status',STATUS_RECEIVED);
		return $this->db->update('contributions');
	}

	public function set_received($id){
		$this->db->set('transference_date', "");
		$this->db->set('status',STATUS_RECEIVED);
		$this->db->where('id',$id);
		$this->db->where('status',STATUS_TRANSFERED);
		return $this->db->update('contributions');
	}
		
	public function set_checked($id){
		$this->db->set('status',STATUS_CHECKED);
		$this->db->where('id',$id);
		$this->db->where('status',STATUS_TRANSFERED);
		return $this->db->update('contributions');
	}

	public function set_transfered_back($id){
		$this->db->set('status',STATUS_TRANSFERED);
		$this->db->where('id',$id);
		$this->db->where('status',STATUS_CHECKED);
		return $this->db->update('contributions');
	}

	public function remove_contribution($id){
		$this->db->where('id',$id);
		$this->db->where('status',STATUS_RECEIVED);
		if ($this->db->delete('contributions')){
			if ($this->db->affected_rows() > 0){
				return true;
			}
		}
		return false;
	}
}
