<?php 
class Log_model extends CI_Model {

	public function check_log_user($email,$pass){
		$query_s = "SELECT id,email,password,name,last_name,role FROM users WHERE email = ?";
		$user_data = $this->db->query($query_s, array($email))->row_array();
		/*echo $pass . ' and ' . $user_data['password'];*/
		if (!empty($user_data) && password_verify($pass,$user_data['password'])){
			unset($user_data['password']);
			return $user_data;	
		}
		else{
			return [];
		}	
	}
		
}
