<?php 
class Settings_model extends CI_Model {

	public function change_password($user_id,$pass){
		$this->db->set('password',password_hash($pass,PASSWORD_DEFAULT));
		$this->db->where('id',$user_id);
		$this->db->update('users');
		return $this->db->affected_rows();
	}
		
	public function generate_retrieval_hash($email){
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('email', $email);
		$user = $this->db->get()->row_array();
		if (!empty($user)){
			do {
				$hash = bin2hex(random_bytes(20));
				$existent_hash = $this->db->select('user_id')->from('password_retrieval')->where('hash', $hash)->get()->row_array();
			} while (!empty($existent_hash));
			if ($this->db->insert("password_retrieval",["user_id"=>$user['id'],"hash"=>$hash])){
				return ["success"=>true,"hash"=>$hash];
			}
			else{
				return ["success"=>false,"error"=>"UNKNOWN_ERROR"];
			}
		}
		else{
			return ["success"=>false,"error"=>"EMAIL_NOT_FOUND"];
		}
	}
		
	public function generate_random_password($hash){
		$this->db->select('user_id');
		$this->db->from('password_retrieval');
		$this->db->where('hash', $hash);
		$user = $this->db->get()->row_array();
		if (!empty($user)){
			$new_password = substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!', mt_rand(1,10))),1,10);
			$this->db->set('password',password_hash($new_password,PASSWORD_DEFAULT));
			$this->db->where('id',$user['user_id']);
			if ($this->db->update('users')){
				$this->db->where('hash',$hash)->delete('password_retrieval');
				return ["success"=>true,"new_password"=>$new_password];
			}
			else{
				return ["success"=>false,"error"=>"UNKNOWN_ERROR"];
			}
		}
		else{
			return ["success"=>false,"error"=>"HASH_NOT_FOUND"];
		}
	}
}
