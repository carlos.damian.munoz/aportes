<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main id="reports">
	<form id="reports-box" method="get" action="">
		<h2>Informes</h2>
		<div>
			<div><label>Umbral</label></div>
			<div><input type="number" name="threshold" required></div>
		</div>
		<div>
			<div><label>Período</label></div>
			<div><label>Desde</label></div>
			<div>
				<select name="month_from">
					<option value="">Mes</option>
					<?php for ($i = 0; $i < 12; $i++) { ?>
					<option value="<?php echo $i+1 ?>"><?php echo $month_names[$i] ?></option>
					<?php } ?>	
				</select>	
				<select name="year_from">
					<option value="">Año</option>
					<?php for ($i = 2011; $i < intval(date("Y"))+YEARS_TO_FUTURE; $i++) { ?>
					<option value="<?php echo $i ?>"><?php echo $i ?></option>
					<?php } ?>	
				</select>	
			</div>
			<div><label>Hasta</label></div>
			<div>
				<select name="month_to">
					<option value="">Mes</option>
					<?php for ($i = 0; $i < 12; $i++) { ?>
					<option value="<?php echo $i+1 ?>"><?php echo $month_names[$i] ?></option>
					<?php } ?>	
				</select>	
				<select name="year_to">
					<option value="">Año</option>
					<?php for ($i = 2011; $i < intval(date("Y"))+YEARS_TO_FUTURE; $i++) { ?>
					<option value="<?php echo $i ?>"><?php echo $i ?></option>
					<?php } ?>	
				</select>	
			</div>
		</div>
		<div>
			<button title="Generar informe">Generar informe</button>
		</div>
	</form>
</main>

