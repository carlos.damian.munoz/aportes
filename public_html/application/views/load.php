<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="notifications">
	<?php if(isset($notifications)) foreach($notifications as $notification){ ?>
	<div class="notification real <?php echo $notification['type']; ?>"><div class="content"><?php echo $notification['content']; ?></div></div>
	<?php } ?>
</div>
<main id="load">

	<form id="load-box" method="post" action="">
		<h2>Cargar aporte</h2>
		<div>
			<div><label>Aportante</label></div>
			<select name="contributor_id" value="" required>
			<option value="">Tipee el nombre...</option>
			<?php foreach($contributors as $contributor) { ?>
				<option value="<?php echo $contributor['id']; ?>"><?php echo $contributor['contributor']; ?></option>
			<?php } ?>	
			</select>
		</div>
		<div>
			<div><label id="monto_label">Monto</label></div>
			<div><input type="number" name="amount" required></div>
		</div>
		<div>
			<div><label>Período</label></div>
			<input type="radio" name="frame_type" class="month" value="month" checked>Mes<input type="radio" name="frame_type" class="range" value="range">Rango
			<div class="frame-month">
				<div>
					<select name="month[month]">
						<option value="">Mes</option>
						<?php for ($i = 0; $i < 12; $i++) { ?>
						<option value="<?php echo $i+1 ?>"><?php echo $month_names[$i] ?></option>
						<?php } ?>	
					</select>	
					<select name="month[year]">
						<option value="">Año</option>
						<?php for ($i = 2011; $i < intval(date("Y"))+YEARS_TO_FUTURE; $i++) { ?>
						<option value="<?php echo $i ?>"><?php echo $i ?></option>
						<?php } ?>	
					</select>	
				</div>
			</div>
			<div class="frame-range">
				<div><label>Desde</label></div>
				<div>
					<select name="range[month_from]">
						<option value="">Mes</option>
						<?php for ($i = 0; $i < 12; $i++) { ?>
						<option value="<?php echo $i+1 ?>"><?php echo $month_names[$i] ?></option>
						<?php } ?>	
					</select>	
					<select name="range[year_from]">
						<option value="">Año</option>
						<?php for ($i = 2011; $i < intval(date("Y"))+YEARS_TO_FUTURE; $i++) { ?>
						<option value="<?php echo $i ?>"><?php echo $i ?></option>
						<?php } ?>	
					</select>	
				</div>
				<div><label>Hasta</label></div>
				<div>
					<select name="range[month_to]">
						<option value="">Mes</option>
						<?php for ($i = 0; $i < 12; $i++) { ?>
						<option value="<?php echo $i+1 ?>"><?php echo $month_names[$i] ?></option>
						<?php } ?>	
					</select>	
					<select name="range[year_to]">
						<option value="">Año</option>
						<?php for ($i = 2011; $i < intval(date("Y"))+YEARS_TO_FUTURE; $i++) { ?>
						<option value="<?php echo $i ?>"><?php echo $i ?></option>
						<?php } ?>	
					</select>	
				</div>
			</div>
		</div>
		<div class="receptionDate">
			<div><label>Recibido el</label></div>
			<input class="date" type="text" name="reception_date" id="reception_date" required>
		</div>
		<div>
			<button title="Cargar aporte">Enviar</button>
		</div>
	</form>
</main>
