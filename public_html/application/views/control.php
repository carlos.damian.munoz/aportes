<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main id="list">
	<div id="remove-confirmation-modal">
		<div class="panel">
			<div class="legend">¿Seguro/a que desea eliminar los aportes seleccionados?</div>
			<div class="buttons-line">
				<button class="yes">Si</button>
				<button class="no">No</button>
			</div>
		</div>
	</div>
	<input type="hidden" id="load_url" value="<?php echo $load_url; ?>">
	<input type="hidden" id="pages_count" value="<?php echo $pages_count; ?>">
	<?php if (!empty($order)) { ?>
		<input type="hidden" id="order_url" value="<?php echo $order['url']; ?>">
		<input type="hidden" id="order_field" value="<?php echo $order['field']; ?>">
		<input type="hidden" id="order_direction" value="<?php echo $order['direction']; ?>">
	<?php } ?>
	<?php if ($historic) { ?>
		<input type="hidden" id="page_name" value="historico">
	<?php } else { ?>
		<input type="hidden" id="page_name" value="seguimiento">
	<?php } ?>
	<div id="selected-filters">	
		<?php foreach($filters as $filter) { ?>
		<div class="filter">
			<span class="name"><?php echo $fields[$filter['name']]['show_name'] . $modifier_alias[$filter['modifier']]; ?> </span>
			<span class="value"><?php echo $filter['show_value']; ?></span>
			<span class="close">&nbsp;</span>
			<input type="hidden" class="filter-entry" value="<?php echo $filter['entry']; ?>">
		</div>
		<?php } ?>	
	</div>
	<div id="filters">	
		<div class="line">
			<div class="filter contributor">
				<div class="title">Aportante</div>
				<input type="hidden" class="url_name" value="aportante">
				<input type="text" class="user_input" list="contributors" value="<?php echo (isset($filters['aportante'])) ? $filters['aportante']['value'] : ""; ?>">
				<datalist id="contributors">
				<?php if ($historic) { ?>
					<?php foreach($contributors as $contributor) { ?>
						<option value="<?php echo $contributor['contributor']; ?>">
					<?php } ?>	
				<?php } else { ?>	
					<?php foreach($user_contributors as $contributor) { ?>
						<option value="<?php echo $contributor['contributor']; ?>">
					<?php } ?>	
				<?php } ?>	
				</datalist>
			</div>
			<div class="filter amount">
				<div class="title">Monto</div>
				<input type="hidden" class="url_name" value="monto">
				<div class="range-container">
					<div>
						<label>Desde</label>
						<input type="number" class="user_input from" value="<?php echo (isset($filters['monto-min'])) ? $filters['monto-min']['value'] : ""; ?>">
					</div>	
					<div>
						<label>Hasta</label>
						<input type="number" class="user_input to" value="<?php echo (isset($filters['monto-max'])) ? $filters['monto-max']['value'] : ""; ?>">
					</div>	
				</div>
			</div>
			<?php if (!$historic) { ?>
			<div class="filter status">
				<div class="title">Estado</div>
				<input type="hidden" class="url_name" value="estado">
				<select>
					<option value="">Estado</option>
					<?php for ($i=$userdata['role']-1; $i < count($status['names']); $i++) { ?>
					<option value="<?php echo ($i+1); ?>" <?php echo (isset($filters['estado']) && $filters['estado']['value'] == ($i+1)) ? "selected" : "" ?>><?php echo $status['names'][$i]; ?></option>
					<?php } ?>
				</select>
			</div>
			<?php } ?>	
			<?php if ($userdata['role'] == ROLES_ADMINISTRATOR || $historic) { ?>
			<div class="filter collector">
				<div class="title">Recaudador</div>
				<input type="hidden" class="url_name" value="recaudador">
				<input type="text" class="user_input" list="collectors" value="<?php echo (isset($filters['recaudador'])) ? $filters['recaudador']['value'] : ""; ?>">
				<datalist id="collectors">
				<?php foreach($collectors as $collector) { ?>
					<option value="<?php echo $collector['collector']; ?>">
				<?php } ?>	
				</datalist>
			</div>
			<?php } ?>	
		</div>
		<div class="line">
			<div class="filter period">
				<div class="title">Período</div>
				<input type="hidden" class="url_name" value="periodo">
				<div class="range-container">
					<div>
						<label>Desde</label>
						<select class="month from">
							<option value="">Mes</option>
							<?php for ($i=0; $i < count($month_names); $i++) { ?>
							<option value="<?php echo ($i+1); ?>" <?php echo (isset($filters['periodo-min']) && intval(explode('-',$filters['periodo-min']['value'])[1]) == ($i+1)) ? "selected" : "" ?>><?php echo $month_names[$i]; ?></option>
							<?php } ?>
						</select>	
						<select class="year from">
							<option value="">Año</option>
							<?php for ($i=2011; $i<=intval(date('Y')); $i++) { ?>
							<option value="<?php echo ($i); ?>" <?php echo (isset($filters['periodo-min']) && intval(explode('-',$filters['periodo-min']['value'])[0]) == ($i)) ? "selected" : "" ?>><?php echo $i; ?></option>
							<?php } ?>
						</select>	
					</div>	
					<div>
						<label>Hasta</label>
						<select class="month to">
							<option value="">Mes</option>
							<?php for ($i=0; $i < count($month_names); $i++) { ?>
							<option value="<?php echo ($i+1); ?>" <?php echo (isset($filters['periodo-max']) && intval(explode('-',$filters['periodo-max']['value'])[1]) == ($i+1)) ? "selected" : "" ?>><?php echo $month_names[$i]; ?></option>
							<?php } ?>
						</select>	
						<select class="year to">
							<option value="">Año</option>
							<?php for ($i=2011; $i<=intval(date('Y'))+YEARS_TO_FUTURE; $i++) { ?>
							<option value="<?php echo ($i); ?>" <?php echo (isset($filters['periodo-max']) && intval(explode('-',$filters['periodo-max']['value'])[0]) == ($i)) ? "selected" : "" ?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</div>	
				</div>
			</div>
			<?php if ($userdata['role'] == ROLES_COLLECTOR && !$historic) { ?>
			<div class="filter receptionDate">
				<div class="title">Fecha de recepción</div>
				<input type="hidden" class="url_name" value="recepcion">
				<div class="range-container">
					<div>
						<label>Desde</label>
						<input id="reception_from" type="text" class="user_input from" value="<?php echo (isset($filters['recepcion-min'])) ? $filters['recepcion-min']['show_value'] : ""; ?>">
					</div>	
					<div>
						<label>Hasta</label>
						<input id="reception_to" type="text" class="user_input to" value="<?php echo (isset($filters['recepcion-max'])) ? $filters['recepcion-max']['show_value'] : ""; ?>">
					</div>	
				</div>
			</div>
			<?php } ?>	
			<div class="filter transferenceDate">
				<div class="title">Fecha de entrega</div>
				<input type="hidden" class="url_name" value="entrega">
				<div class="range-container">
					<div>
						<label>Desde</label>
						<input id="transference_from" type="text" class="user_input from" value="<?php echo (isset($filters['entrega-min'])) ? $filters['entrega-min']['show_value'] : ""; ?>">
					</div>	
					<div>
						<label>Hasta</label>
						<input id="transference_to" type="text" class="user_input to" value="<?php echo (isset($filters['entrega-max'])) ? $filters['entrega-max']['show_value'] : ""; ?>">
					</div>	
				</div>
			</div>
		</div>
		<button class="filter-action" title="Aplicar filtros">Aplicar</button>
	</div>

	<div class="header">
		<button class="filters-button" title="Mostrar / ocultar filtros">Filtros</button>
		<div class="buttons">
			<?php if (!$historic) { ?>
				<?php if ($userdata['role'] == ROLES_COLLECTOR) { ?>
					<button disabled="disabled" title="Eliminar los aportes seleccionados" id="remove">Eliminar</button>	
					<button disabled="disabled" title="Marcar los aportes seleccionados como recibidos" id="set_received">Marcar como no entregado</button>	
					<button disabled="disabled" title="Marcar los aportes seleccionados como entregados" id="set_transfered">Marcar como entregado</button>	
				<?php } elseif ($userdata['role'] == ROLES_ADMINISTRATOR) { ?>
					<button disabled="disabled" title="Marcar los aportes seleccionados como entregados" id="set_transfered_back">Marcar como no confirmado</button>	
					<button disabled="disabled" title="Marcar los aportes seleccionados como confirmados" id="set_checked">Marcar como confirmado</button>	
				<?php } ?>
			<?php } ?>
		</div>
		<div class="results-legend">
			<div><span class="total-results-number"><?php echo $total_results_number; ?></span> resultados (mostrando <span class="results-shown-number">0</span>)</div>
		</div>
	</div>	

	<div class="notifications">
		<div class="notification dummy"><div class="content">Dummy notification</div></div>
		<?php if(isset($notifications)) foreach($notifications as $notification){ ?>
		<div class="notification real <?php echo $notification['type']; ?>"><div class="content"><?php echo $notification['content']; ?></div></div>
		<?php } ?>
	</div>

	<div id="contributions">
		<div class="elements-header">
			<div class="checkbox">
				<input type="checkbox" title="Seleccionar / deseleccionar todos los aportes visibles no confirmados">
			</div>
			<?php if ($userdata['role'] == ROLES_ADMINISTRATOR || $historic) { ?>
			<div class="collector"><span class="recaudador" title="Ordenar alfabéticamente por nombre de recaudador">Recaudador</span></div>
			<?php } ?>
			<div class="contributor"><span class="aportante" title="Ordenar alfabéticamente por nombre de aportante">Aportante</span></div>
			<div class="amount"><span class="monto" title="Ordenar por monto">Monto</span></div>
			<div class="period"><span class="periodo" title="Ordenar cronológicamente por período">Período</span></div>
			<?php if ($userdata['role'] == ROLES_COLLECTOR && !$historic) { ?>
			<div class="receptionDate"><span class="recepcion" title="Ordenar cronológicamente por fecha de recepción">Recepción</span></div>
			<?php } ?>
			<div class="transferenceDate"><span class="entrega" title="Ordenar cronológicamente por fecha de entrega">Entrega</span></div>
			<?php if (!$historic) { ?>
			<div class="status"><span class="estado" title="Ordeanar por estado">Estado</span></div>
			<?php } ?>
		</div>
	</div>
	<div class="ajax-loader">
		<img src="/assets/images/ajax-loader.gif">
	</div>
</main>
