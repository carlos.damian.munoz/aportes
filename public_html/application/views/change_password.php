<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
	<?php if (isset($success)) { ?> 
	<div class="success-message"><?php echo $success; ?></div>
	<?php } else { ?>
	<form id="login-box" method="post" action="">
		<h2>Cambiar contraseña</h2>
		<div>
			<input type="password" placeholder="contraseña actual" name="password" value="">
		</div>
		<div>
			<input type="password" placeholder="nueva contraseña" name="new_password" pattern=".{6,}" required title="mínimo 6 caracteres">
		</div>
		<div>
			<input type="password" placeholder="repetir nueva contraseña" name="repeat_new_password" pattern=".{6,}" required title="mínimo 6 caracteres">
		</div>
		<div>
			<button title="Cambiar">Cambiar</button>
		</div>
	</form>
	<?php } ?>
	<?php if (isset($error)) { ?>
		<div class="error-message <?php echo $error['class']; ?>"><?php echo $error['message']; ?></div>
	<?php } ?>
</main>


