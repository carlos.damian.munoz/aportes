<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf8">
		<title>Cargar aporte</title>
		<link rel="stylesheet" href="/assets/styles/layout.css">
		<?php foreach ($stylesheets as $path){ ?>
			<link rel="stylesheet" href="/assets/styles/<?php echo $path; ?>">
		<?php } ?>
		<link rel="stylesheet" href="/assets/js/jquery-ui-1.12.1.custom/jquery-ui.css">
		<script src="/assets/js/jquery-3.2.1.min.js"></script>
		<script src="/assets/js/jquery-ui.min.js"></script>
		<script src="/assets/js/layout.js"></script>
		<?php foreach ($scripts as $path){ ?>
			<script src="/assets/js/<?php echo $path; ?>"></script>
		<?php } ?>
	</head>
	<body>	
		<?php if(isset($userdata)){ ?>
			<div class="login-bar">
				<a href="/modificar_clave" title="Modificar contraseña"><img src="/assets/images/change_password.png"></a>
				<span class="user-name"><?php echo ucwords($userdata['name']) . ' ' . ucwords($userdata['last_name']); ?></span>
				<a href="/logout" title="Cerrar Sesión">Salir</a>
			</div>
		<?php } ?>
		<header>
			<h1>Aportes</h1>
			<nav>
				<?php if(isset($userdata)){ ?>
					<?php if($userdata['role'] < 2 ){ ?>
					<a class="load" href="/carga" title="Cargar nuevos aportes">Carga</a>
					<?php } ?>
					<a class="list" href="/seguimiento/o_id_desc" title="Seguimiento de aportes">Seguimiento</a>
					<?php if($userdata['role'] < 2 ){ ?>
					<a class="historic" href="/historico" title="Ver todos los aportes">Histórico</a>
					<?php } ?>
					<a class="reports" href="/informes" title="Obtener informes">Informes</a>
				<?php } ?>
			</nav>
		</header>
		<?php echo $main; ?>
		<footer>
			<span>Equipo de aportes de parque la reja</span> 
		</footer>
	</body>
</html>
