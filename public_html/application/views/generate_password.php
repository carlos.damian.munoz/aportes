<main>
<?php if (isset($new_password)) { ?>
<div>
	Su contraseña temporal es <?php echo $new_password; ?><br>
	ATENCIÓN! Esta contraseña expirará en 7 días. Cambie su contraseña ingresando al link que se encuentra al lado de su nombre.
</div>
<a class="blue-link" href="/login" title="Iniciar sesión">Iniciar sesión</a>
<?php } else if (isset($error)) { ?>
<div>
	<?php echo $error['message']; ?>
</div>
<?php } ?>
</main>
