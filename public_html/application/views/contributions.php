<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php foreach ($contributions as $contribution) { ?>
<div class="element">
	<input type="hidden" class="id" value="<?php echo $contribution['id']; ?>">
	<div class="checkbox">
		<?php if ( ($contribution['status'] != STATUS_CHECKED && $userdata['role'] == ROLES_COLLECTOR) || 
		($userdata['role'] == ROLES_ADMINISTRATOR) || $contribution['status'] == STATUS_RECEIVED ) { ?>
		<input type="checkbox">
		<?php } ?>
	</div>
	<?php if ($userdata['role'] == ROLES_ADMINISTRATOR || $historic) { ?>
	<div class="collector"><span><?php echo $contribution['collector']; ?></span></div>
	<?php } ?>
	<div class="contributor"><span><?php echo $contribution['contributor']; ?></span></div>
	<div class="amount"><span><?php echo $contribution['amount']; ?></span></div>
	<div class="period"><span><?php echo $month_names[$contribution['period_month']-1] . ' ' . $contribution['period_year']; ?></span></div>
	<?php if ($userdata['role'] == ROLES_COLLECTOR && !$historic) { ?>
	<div class="receptionDate"><?php echo $contribution['fecha_recepcion']; ?><span></span></div>
	<?php } ?>
	<div class="transferenceDate"><span><?php echo $contribution['fecha_transferencia']; ?></span></div>
	<?php if (!$historic) { ?>
	<div class="status <?php echo $status['classes'][$contribution['status']-1]; ?>">
		<span title="<?php echo $status['names'][$contribution['status']-1] . ": " . $status['titles'][$contribution['status']-1]; ?>">
			<?php echo $status['names'][$contribution['status']-1]; ?>
		</span>
	</div>
	<?php } ?>
</div>
<?php } ?>
